﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Core.Handlers;
using TelegramCore.Handlers;

namespace TelegramCore
{
    public class BotCore
    {
        TelegramBotClient client;

        public BotCore()
        {
            client = new TelegramBotClient("1122456426:AAHP1Q5oOsda8fSS-7b3lG1ugpZXNhIHj6A");
        }

        public void Start()
        {
            client.StartReceiving();

            client.OnMessage += Client_OnMessage;
            client.OnCallbackQuery += Client_OnCallbackQuery;

        }

        private void Client_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            var handleFactory = new HandlerFactory();
            var handler = handleFactory.GetHandler(e.Message, client);
            handler.Handle(e.Message);
        }

        private void Client_OnCallbackQuery(object sender, Telegram.Bot.Args.CallbackQueryEventArgs e)
        {
            var handleFactory = new CallbackHandlerFactory();
            var handler = handleFactory.GetHandler(e.CallbackQuery, client);

            handler.Handle(e.CallbackQuery);
        }

        public void Stop()
        {
            client.StopReceiving();
        }

    }
}
