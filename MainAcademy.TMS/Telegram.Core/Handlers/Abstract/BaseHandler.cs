﻿using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramCore.Handlers.Abstract
{
    public abstract class BaseHandler
    {
        protected readonly UserRepository userRepository;
        protected readonly ProfessionRepository professionRepository;
        protected readonly TaskRepository taskRepository;

        protected readonly TelegramBotClient telegramBotClient;

        public BaseHandler(TelegramBotClient telegramBotClient)
        {
            userRepository = new UserRepository();
            professionRepository = new ProfessionRepository();
            taskRepository = new TaskRepository();

            this.telegramBotClient = telegramBotClient;
        }

        public virtual void Handle(Message message)
        {

        }

        public virtual void Handle(CallbackQuery message)
        {

        }

    }
}
