﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramCore.Handlers.Abstract;

namespace Telegram.Core.Handlers.Concrete
{
    class TaskStatusChangeHandler : BaseHandler
    {
        public TaskStatusChangeHandler(TelegramBotClient telegramBotClient)
            : base(telegramBotClient)
        {

        }

        public override void Handle(CallbackQuery message)
        {
            // todo
            string[] array = message.Data.Split(new char[] { ':' });
            var taskidd = Convert.ToInt32(array[1]);
            var taskstatusid = Convert.ToInt32(array[2]);

            //var tas = taskRepository.GetTask(int.Parse(taskidd));
            //tas.TaskStatusId = (int.Parse(taskiddl));


           taskRepository.ChangeTelegramTaskStatus(taskidd, taskstatusid);
            

            //var task = taskRepository.GetTasks();
            //var taski = taskRepository.GetTask(int.Parse(taskidd));
            

            telegramBotClient.SendTextMessageAsync(
              new ChatId(message.Message.Chat.Id), $"Status Changed");

        }
    }
}
