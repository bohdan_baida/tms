﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramCore.Handlers.Abstract;

namespace Telegram.Core.Handlers.Concrete
{
    public class ListHandler : BaseHandler
    {
        public ListHandler(Telegram.Bot.TelegramBotClient telegramBotClient)
            : base(telegramBotClient)
        {

        }
        public override void Handle(Message message)
        {

            var tasks = taskRepository.GetUserTasks(userRepository.GetUser(message.From.Username).Id);

            foreach (var task in tasks)
            {
                var keyboard = new InlineKeyboardMarkup(new[]
            {
                new InlineKeyboardButton()
                {
                    Text = "Change status",
                    CallbackData = $"StatusChange:{task.Id}"
                },
                new InlineKeyboardButton()
                {
                    Text = "Change user",
                    CallbackData = $"UserChange:{task.Id}"
                }
            });

                telegramBotClient.SendTextMessageAsync(
                   new ChatId(message.Chat.Id), $"Header: {task.TaskHeader}, Description: {task.TaskDesc}", replyMarkup: keyboard);

            }
        }

        public override void Handle(CallbackQuery message)
        {
            string[] messageInform = message.Data.Split(new char[] { ':' });
            string actionName = messageInform.First();
            string taskId = messageInform.Last();

            if (actionName == "StatusChange")
            {
                var taskStatuses = taskRepository.GetTaskStatuses();

                var keyboard = taskStatuses.Select(taskStatus => new InlineKeyboardButton()
                {
                    Text = $"{taskStatus.Name}",
                    CallbackData = $"TaskStatusChange:{taskId}:{taskStatus.Id}"
                })
                .ToArray();

                telegramBotClient.SendTextMessageAsync(
                  new ChatId(message.Message.Chat.Id), $"Select new status",
                  replyMarkup: new InlineKeyboardMarkup(keyboard));
            }

            if (actionName == "UserChange")
            {
                var users = userRepository.GetUsersForTheTask(1);

                var keyboard = users.Select(user => new InlineKeyboardButton()
                {
                    Text = $"{user.UserName}",
                    CallbackData = $"TaskUserChange:{taskId}:{user.Id}"
                })
                .ToArray();

                telegramBotClient.SendTextMessageAsync(
                  new ChatId(message.Message.Chat.Id), $"Select new user",
                  replyMarkup: new InlineKeyboardMarkup(keyboard));
            }


        }
    }
}
