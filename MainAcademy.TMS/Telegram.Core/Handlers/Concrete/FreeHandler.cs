﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramCore.Handlers.Abstract;

namespace Telegram.Core.Handlers.Concrete
{
    class FreeHandler : BaseHandler
    {
        public FreeHandler(Telegram.Bot.TelegramBotClient telegramBotClient)
                  : base(telegramBotClient)
        {

        }

        public override void Handle(Message message)
        {
            telegramBotClient.SendTextMessageAsync(
                    new ChatId(message.Chat.Id), "Choose another function!", replyMarkup: null);
        }
    }
}