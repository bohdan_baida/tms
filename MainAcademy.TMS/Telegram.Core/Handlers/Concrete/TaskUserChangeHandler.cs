﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramCore.Handlers.Abstract;

namespace Telegram.Core.Handlers.Concrete
{
    class TaskUserChangeHandler : BaseHandler
    {
        public TaskUserChangeHandler(TelegramBotClient telegramBotClient)
           : base(telegramBotClient)
        {

        }

        public override void Handle(CallbackQuery message)
        {
            // todo
            string[] array = message.Data.Split(new char[] { ':' });
            var taskId = Convert.ToInt32(array[1]);
            var userId = Convert.ToInt32(array[2]);

            taskRepository.ChangeTelegramTaskUser(taskId, userId);

            telegramBotClient.SendTextMessageAsync(
              new ChatId(message.Message.Chat.Id), $"User Changed");

        }
    }
}