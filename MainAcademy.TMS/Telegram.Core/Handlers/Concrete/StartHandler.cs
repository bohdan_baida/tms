﻿using CoreEntities;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramCore.Handlers.Abstract;

namespace TelegramCore.Handlers.Concrete
{
    public class StartHandler : BaseHandler
    {

        public StartHandler(Telegram.Bot.TelegramBotClient telegramBotClient)
            : base(telegramBotClient)
        {

        }
        public override void Handle(Message message)
        {
            var userName = message.From.Username;

            var user = userRepository.GetUser(userName);

            if (user == null)
            {
                userRepository.AddUser(new CoreEntities.User()
                {
                    TelegramUserName = userName,
                    UserName = userName,
                    Person = new CoreEntities.Person()
                    {
                        FirstName = message.From.FirstName,
                        LastName = message.From.LastName

                    }
                });
            }

            if (user.UserProfessions.Count == 0)
            {
                List<KeyboardButton>[] btns = ReturnProfessionsButtons();

                var keyboard = new ReplyKeyboardMarkup(btns.Select(c => c.ToArray()).ToArray());
                telegramBotClient.SendTextMessageAsync(
                    new ChatId(message.Chat.Id), "Please select your professions",
                    replyMarkup: keyboard);
                //add keyboard
            }


        }

        private static List<KeyboardButton>[] ReturnProfessionsButtons()
        {
            ProfessionRepository professionRepository = new ProfessionRepository();
            List<Profession> professions = professionRepository.GetProfessions().ToList();

            List<KeyboardButton>[] btns = new List<KeyboardButton>[professions.Count];
            for (int i = 0; i < professions.Count; i++)
            {
                btns[i] = new List<KeyboardButton>();
                btns[i].Add(new KeyboardButton()
                {
                    Text = professions[i].Name
                });
            }

            return btns;
        }
    }
}
