﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramCore.Handlers.Abstract;

namespace TelegramCore.Handlers.Concrete
{
    public class ProfessionHandler : BaseHandler
    {
        public ProfessionHandler(Telegram.Bot.TelegramBotClient telegramBotClient)
            : base(telegramBotClient)
        {

        }

        public override void Handle(Message message)
        {
            var user = userRepository.GetUser(message.From.Username);

            if (user == null)
                return;

            var profession = professionRepository.GetProfession(message.Text);

            userRepository.AddProfession(user, profession);
        }
    }
}
