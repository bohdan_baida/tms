﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramCore.Handlers.Abstract;


namespace Telegram.Core.Handlers.Concrete
{
    public class AddHandler : BaseHandler
    {
        public AddHandler(Telegram.Bot.TelegramBotClient telegramBotClient)
            : base(telegramBotClient)
        {

        }
        
        public override void Handle(Message message)
        {
            telegramBotClient.SendTextMessageAsync(
                    new ChatId(message.Chat.Id), "Task Added");

            taskRepository.AddTask(new CoreEntities.Task()
            {
                TaskHeader = message.Text.Substring(4),
                TaskDesc = " ",
                CreationDate = DateTime.Now,
                CreatedBy = userRepository.GetUser(message.From.Username).Id,
                TaskStatusId = (int)Enums.TaskStatus.New
            });
            
        }
    }
}
