﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Core.Handlers.Concrete;
using TelegramCore.Handlers.Abstract;

namespace Telegram.Core.Handlers
{
    class CallbackHandlerFactory
    {
        public CallbackHandlerFactory()
        {
        }

        public BaseHandler GetHandler(CallbackQuery message, Bot.TelegramBotClient telegramBotClient)
        {
            if (message.Data.StartsWith("StatusChange") || message.Data.StartsWith("UserChange"))
            {
                return new ListHandler(telegramBotClient);
            }

            if (message.Data.StartsWith("TaskStatusChange"))
            {
                return new TaskStatusChangeHandler(telegramBotClient);
            }

            if (message.Data.StartsWith("TaskUserChange"))
            {
                return new TaskUserChangeHandler(telegramBotClient);
            }
            return new FreeHandler(telegramBotClient);
        }
    }
}
