﻿using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Core.Handlers.Concrete;
using TelegramCore.Handlers.Abstract;
using TelegramCore.Handlers.Concrete;

namespace TelegramCore.Handlers
{
    public class HandlerFactory
    {
        protected readonly ProfessionRepository professionRepository;

        public HandlerFactory()
        {
            professionRepository = new ProfessionRepository();
        }

        public BaseHandler GetHandler(Message message, Telegram.Bot.TelegramBotClient telegramBotClient)
        {
            if (message.Text == "/start")
            {
                return new StartHandler(telegramBotClient);
            }

            var professions = professionRepository.GetProfessions();
            if (professions.Any(c => c.Name == message.Text))
            {
                return new ProfessionHandler(telegramBotClient);
            }

            if (message.Text.StartsWith("/add"))
            {
                return new AddHandler(telegramBotClient);
            }

            if (message.Text == "/list")
            {
                return new ListHandler(telegramBotClient);
            }

            return new FreeHandler(telegramBotClient); ;
        }
    }
}
