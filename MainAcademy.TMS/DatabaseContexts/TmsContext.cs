namespace DatabaseContexts
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using CoreEntities;

    public partial class TmsContext : DbContext
    {
        
        public TmsContext()
            : base("name=TmsContext")
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskStatus> TaskStatuses { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .Property(e => e.CommentText)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .HasMany(e => e.Comments1)
                .WithOptional(e => e.Comment1)
                .HasForeignKey(e => e.ParentCommentId);

            modelBuilder.Entity<Person>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Task>()
                .Property(e => e.TaskHeader)
                .IsUnicode(false);

            modelBuilder.Entity<Task>()
                .Property(e => e.TaskDesc)
                .IsUnicode(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Tasks1)
                .WithOptional(e => e.Task1)
                .HasForeignKey(e => e.TaskParentId);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Tasks1)
                .Map(m => m.ToTable("UserTasks").MapLeftKey("TaskId").MapRightKey("UserId"));

            modelBuilder.Entity<TaskStatus>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<TaskStatus>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.TaskStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.TelegramUserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);
        }
    }
}
