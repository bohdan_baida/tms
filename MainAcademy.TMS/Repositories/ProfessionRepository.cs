﻿using CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class ProfessionRepository : BaseRepository
    {
        public Profession GetProfession(string name)
        {
            return context.Professions.FirstOrDefault(c => c.Name == name);
        }

        public IEnumerable<Profession> GetProfessions()
        {
            return context.Professions.ToList();
        }
    }
}
