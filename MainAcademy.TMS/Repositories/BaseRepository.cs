﻿using CoreEntities;

namespace Repositories
{
    public class BaseRepository
    {
        protected TmsContext context;

        public BaseRepository(TmsContext context)
        {

        }
        public BaseRepository()
        {
            context = new TmsContext();
        }
    }
}
