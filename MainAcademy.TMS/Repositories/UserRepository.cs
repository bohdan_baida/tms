﻿using CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class UserRepository : BaseRepository
    {
        public User GetUser(string telegramUserName)
        {
            return context.Users.FirstOrDefault(c => c.TelegramUserName == telegramUserName);
        }
        public List<User> GetUsersForTheTask(int idAreasOfProfessionalActivity)
        {
           // List<Profession> prof = context.Professions.Where(
            //x => x.AreaOfProfessionalActivityId == idAreasOfProfessionalActivity).ToList();
            //return context.Users.Where(c => c.UserProfessions.Contains(prof));
           return context.Users.Where(c => c.UserProfessions.Any(x => x.Profession.AreaOfProfessionalActivityId == idAreasOfProfessionalActivity)).ToList();
            //return context.Users.ToList();
        }

        public void AddUser(User user)
        {
            context.Users.Add(user);

            context.SaveChanges();
        }

        public void AddProfession(User user, Profession profession)
        {
            user.UserProfessions.Add(new UserProfession()
            {
                UserId = user.Id,
                ProfessionId = profession.Id
            });
            context.SaveChanges();
        }

        public User GetTheMostFreeExpert(AreasOfProfessionalActivity areaOfProfessionalActivity)
        {
            var listUserTasks = from user in context.Users
                                join task in context.Tasks on user.Id equals task.User.Id
                                select new { userRes = user, taskRef = task };

            int[] tasksExcluded = new int[] {
                (int)Enums.TaskStatus.Closed,
                (int)Enums.TaskStatus.Obsolete };
            var listUserTasksGroup = from userTasks in listUserTasks//.ToList()
                                     group userTasks by userTasks.userRes into userTasksGroop
                                     select new
                                     {
                                         userRef = userTasksGroop.Key,
                                         taskCount = userTasksGroop
                                            .Where(c => !tasksExcluded.Contains(c.taskRef.TaskStatusId)).Count()
                                     };

            //var mostFreeUser = context.Users
            //    .OrderBy(c => c.Tasks
            //        .Where(x => !tasksExcluded.Contains(x.TaskStatusId))
            //        .Count())
            //    .FirstOrDefault();



            return listUserTasksGroup.ToList().OrderBy(c => c.taskCount).FirstOrDefault()?.userRef;
        }

        //public User GetSuitableUser(int areaOfProfessionalActivityId)
        //{
        //    //var users = context.Professions.Where(c => c.AreaOfProfessionalActivityId == areaOfProfessionalActivityId)
        //    //    .SelectMany(c => c.UserProfessions)
        //    //    .Select(c=>c.User)

        //}
    }
}
