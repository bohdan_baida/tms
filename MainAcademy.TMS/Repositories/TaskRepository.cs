﻿using CoreEntities;
using RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositories
{
    public class TaskRepository : BaseRepository, ITaskRepository
    {

        public void ChangeTaskStatus(int taskId, Enums.TaskStatus taskStatus)
        {
            var task = GetTask(taskId);
            task.TaskStatusId = (int)taskStatus;
            context.SaveChanges();
        }

        public void ChangeTelegramTaskStatus(int taskId, int taskStatus)
        {
            var task = GetTask(taskId);
            task.TaskStatusId = taskStatus;
            context.SaveChanges();
        }

        public void ChangeTelegramTaskUser(int taskId, int userId)
        {
            var task = GetTask(taskId);
            task.User = GetUser(userId);
            context.SaveChanges();
        }

        public void AddTask(CoreEntities.Task task)
        {
            context.Tasks.Add(task);
            context.SaveChanges();
        }

        public CoreEntities.Task GetTask(int id)
        {
            return context.Tasks.Single(c => c.Id == id);
        }
        public List<CoreEntities.Task> GetUserTasks(int userid)
        {
            return context.Tasks.Where(c => c.User.Id == userid).ToList();
        }

        public CoreEntities.User GetUser(int id)
        {
            return context.Users.Single(c => c.Id == id);
        }

        public IEnumerable<int> GetTaskUsersIds(int taskId)
        {
            throw new NotImplementedException();
        }

        public void AddTaskToUser(int taskId, int userId)
        {

            try
            {
                CoreEntities.Task task = GetTask(taskId);
                CoreEntities.User user = GetUser(userId);

                task.Users.Add(user);

                context.SaveChanges();
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void ClosedTask(int taskId)
        {
            try
            {
                CoreEntities.Task task = GetTask(taskId);
                task.TaskStatusId = (int)Enums.TaskStatus.Closed;
                context.SaveChanges();
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Task> GetTasks()
        {
            return context.Tasks.ToList();
        }
        public IEnumerable<TaskStatus> GetTaskStatuses()
        {
            return context.TaskStatuses.ToList();
        }

    }
}
