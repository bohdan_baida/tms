﻿using CoreEntities;
using Enums;
using RepositoryInterfaces;
using ServiceInterfaces;
using ServiceInterfaces.Extrernal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices
{
    public class TaskService : ITaskService
    {
        ITaskRepository taskRepository;
        ITelegramService telegramService;

        public TaskService(ITaskRepository taskRepository, ITelegramService telegramService)
        {
            this.taskRepository = taskRepository;
            this.telegramService = telegramService;
        }

        public void AddTaskToUser(int taskId, int userId)
        {
            taskRepository.AddTaskToUser(taskId, userId);
        }
        public void AddTask(CoreEntities.Task task)
        {
            taskRepository.AddTask(task);
        }
        public void ChangeTaskStatus(int taskId, Enums.TaskStatus taskStatus)
        {
            taskRepository.ChangeTaskStatus(taskId, taskStatus);
            var taskUsers = taskRepository.GetTaskUsersIds(taskId);
            telegramService.Notify(taskUsers, "Task status is changed...");
        }
        public void ClosedTask(int taskId)
        {
            taskRepository.ClosedTask(taskId);
        }
        public CoreEntities.Task GetTask(int taskId)
        {
            return taskRepository.GetTask(taskId);
        }
    }
}
