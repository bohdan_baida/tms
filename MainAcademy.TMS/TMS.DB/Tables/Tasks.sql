﻿CREATE TABLE Tasks(
	Id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	TaskHeader varchar(80) NOT NULL,
	TaskDesc varchar(255) NULL,
	TaskStatusId int NOT NULL,
	CreationDate datetime NOT NULL,
	CreatedBy int NOT NULL,
	LastStatusDate datetime NULL,
	TaskParentId int NULL,
	AreaOfProfessionalActivityId INT null
	FOREIGN KEY (TaskParentId) REFERENCES Tasks(Id)
	FOREIGN KEY (CreatedBy) REFERENCES dbo.Users(Id)
	FOREIGN KEY (TaskStatusId) REFERENCES dbo.TaskStatuses(Id)
	FOREIGN KEY (AreaOfProfessionalActivityId) REFERENCES dbo.AreasOfProfessionalActivity(Id)
	)