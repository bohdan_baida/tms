﻿CREATE TABLE [dbo].[Professions]
(
	Id int NOT NULL primary key,
	Name VARCHAR(max) NOT NULL,
	Description VARCHAR(max),
	AreaOfProfessionalActivityId INT NOT NULL
	FOREIGN KEY (AreaOfProfessionalActivityId) REFERENCES dbo.AreasOfProfessionalActivity(Id)
)
