﻿CREATE TABLE [dbo].[Persons]
(
	Id int not null primary key identity(1,1),
	FirstName varchar(max),
	LastName varchar(Max),
	NotRelevant BIT NOT NULL DEFAULT 0,
	OrganizationID int 
	/*FOREIGN KEY (OrganizationID) REFERENCES dbo.Organizations(Id)*/
)
