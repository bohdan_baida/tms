﻿CREATE TABLE [dbo].[AreasOfProfessionalActivity]
(
	Id int NOT NULL primary key,
	Name VARCHAR(max) NOT NULL,
	Description VARCHAR(max)
)
