﻿CREATE TABLE [dbo].[Users]
(
	Id int not null primary key identity(1,1),
	UserName varchar(100) not null,
	Email nvarchar(256) NULL,
	PhoneNumber nvarchar(max) NULL,
	PersonId INT,
	[TelegramUserName] VARCHAR(MAX) NULL, 
    UNIQUE (UserName),
	FOREIGN KEY (PersonId) REFERENCES dbo.Persons(Id)
)
