﻿CREATE TABLE [dbo].[UserProfessions]
(
	[UserId] INT NOT NULL,
	[ProfessionId] int not null,

	constraint pk_userprofessions_userId_profession_id primary key(UserId, ProfessionId),
	constraint fk_userprofessions_userId_users foreign key (UserId) references dbo.Users(Id),
	constraint fk_userprofessions_professionid_PROFESSIONS foreign key (UserId) references dbo.Professions(Id),



)
