﻿CREATE TABLE UserTasks(
UserId int not null,
TaskId int not null
PRIMARY KEY(UserId,TaskId),
FOREIGN KEY (UserId) references dbo.Users(Id),
FOREIGN KEY (TaskId) references dbo.Tasks(Id),
)