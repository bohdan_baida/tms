﻿CREATE TABLE [dbo].[TaskStatuses]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR(100)
)
