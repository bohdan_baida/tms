﻿create table [dbo].[Comments]
(
Id int not null primary key identity(1,1),
TaskId int not null,
CommentDate datetime not null,
UserId int not null,
CommentText varchar(max)not null,
[ParentCommentId] int,
FOREIGN KEY ([ParentCommentId]) REFERENCES [Comments]([Id])
--FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]),
--FOREIGN KEY ([TaskId]) REFERENCES [Tasks]([Id]),
)