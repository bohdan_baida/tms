﻿using CoreEntities;
using Enums;
using ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        ITaskService taskService;

        public HomeController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Task(int taskId)
        {
            //var task = taskService.GetTask(taskId);
            //return View(task);
            return View(new Task()
            {
                Id=1,
                TaskHeader = "Cleanup the room!",
                TaskDesc = "dsgdfgdfgdgfdgdfsdfksdkflsdksaklsa;d\ndsfdfdfdfdfdfdfd",
                Comments = new List<Comment>()
                {
                    new Comment()
                    {
                        Id=1,
                        CommentText = "dsadsadsadsadsa"
                    },
                    new Comment()
                    {
                        Id=1,
                        CommentText = ".,mfdsadfghj;kjhgfdscadfghjk"
                    }
                }
            });
        }

        [HttpPost]
        public ActionResult AddTask(CoreEntities.Task task)
        {
            try
            {
                taskService.AddTask(task);
            }
            catch (Exception e)
            {
                return View(e);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddTaskToUser(int taskId, int userId)
        {
            try
            {
                taskService.AddTaskToUser(taskId, userId);
            }
            catch (Exception ex)
            {
                return View(ex);
            }

            return View();

        }
        [HttpPost]
        public ActionResult AttachProffesionToTask()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ClosedTask(int taskId)
        {
            try
            {
                taskService.ClosedTask(taskId);
            }
            catch (Exception ex)
            {
                return View(ex);
            }

            return View();
        }

        [HttpPost]
        public ActionResult ChangeTaskStatus(int taskId, Enums.TaskStatus taskStatus)
        {
            try
            {
                taskService.ChangeTaskStatus(taskId, taskStatus);
            }
            catch (Exception ex)
            {
                return View(ex);
            }

            return View();
        }
    }
}