﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    public enum TaskStatus
    {
        New = 1,
        Active = 2,
        Done = 3,
        Closed = 4,
        Obsolete = 5

    }
}
