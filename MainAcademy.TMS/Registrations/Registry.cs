﻿using Ninject.Modules;
using Repositories;
using RepositoryInterfaces;

namespace Registrations
{
    public class Regirstry : NinjectModule
    {
        public override void Load()
        {
            Bind<ITaskRepository>().To<TaskRepository>();
            Bind<ITaskService>().To<TaskService>();
            Bind<ITelegramService>().To<TelegramService>();

        }
    }
}
