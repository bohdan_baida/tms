﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramCore;

namespace Bot
{
    class Program
    {
        static void Main(string[] args)
        {
            BotCore botCore = new BotCore();
            botCore.Start();

            Console.Read();
        }
    }
}
