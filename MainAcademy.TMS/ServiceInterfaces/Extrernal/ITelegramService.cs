﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceInterfaces.Extrernal
{
    public interface ITelegramService
    {
        void Notify(IEnumerable<int> userId, string message);
    }
}
