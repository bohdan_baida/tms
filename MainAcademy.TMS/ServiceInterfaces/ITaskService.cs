﻿using CoreEntities;

namespace ServiceInterfaces
{
    public interface ITaskService
    {
        void ChangeTaskStatus(int taskId, Enums.TaskStatus taskStatus);
        void AddTaskToUser(int taskId, int userId);
        void ClosedTask(int taskId);
        Task GetTask(int taskId);
        void AddTask(CoreEntities.Task task);
    }
}
