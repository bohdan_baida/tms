﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryInterfaces
{
    public interface ITaskRepository
    {
        CoreEntities.Task GetTask(int id);
        void ChangeTaskStatus(int taskId, Enums.TaskStatus taskStatus);
        void AddTaskToUser(int taskId, int userId);
        void ClosedTask(int taskId);
        IEnumerable<int> GetTaskUsersIds(int taskId);
        void AddTask(CoreEntities.Task task);
    }
}
