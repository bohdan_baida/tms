﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace TelegramBot
{
    class Program
    {   
        static void Main(string[] args)
        {
            TelegramCore.BotCore bot = new TelegramCore.BotCore();

            bot.Start();

            Console.Read();
        }
    }
}
